FROM node:8

WORKDIR /home/node/app

COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

VOLUME /home/node/app

COPY . .

EXPOSE 8080
CMD [ "npm", "start" ]
RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "vim"]