var assert = require('assert');
var ThousandsSeparatorRegExp = require("./../src/ThousandsSeparatorRegExp.js");
var Test = new ThousandsSeparatorRegExp();

describe("Testa separador por RegExp",function(){
    it("2 dígitos",function(){
        assert(Test.format(39) == '39');
    });

    it("3 dígitos",function(){
        assert(Test.format(399) == '399');
    });

    it("4 dígitos",function(){
        assert(Test.format(3999) == '3,999');
    });

    it("5 dígitos",function(){
        assert(Test.format(12345) == '12,345');
    });

    it("6 dígitos",function(){
        assert(Test.format(123456) == '123,456');
    });

    it("7 dígitos",function(){
        assert(Test.format(7399999) == '7,399,999');
    });

    it("8 dígitos",function(){
        assert(Test.format(23456789) == '23,456,789');
    });

    it("9 dígitos",function(){
        assert(Test.format(123456789) == '123,456,789');
    });

    it("Outro separador",function(){
        assert(Test.format(7399999, '||') == '7||399||999');
    });

    it("Outro tipo",function(){
        assert(Test.format(null) == '0');
    });

    it("Negativo",function(){
        assert(Test.format(-4000) == '-4,000');
    });

    it("16 dígitos",function(){
        assert(Test.format(12345678901234567) == '0');
    });
});