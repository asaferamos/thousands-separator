const ThousandsSeparator = require('./ThousandsSeparator.js');

class ThousandsSeparatorSubtract extends ThousandsSeparator {
    format(value,separator){
        separator = super.checkSeparator(separator);
        if(super.checkValue(value) == 0)
            return 0;

        var negative = '';

        if(value[0] == '-'){
            negative = '-';
            value = value.substring(1);
        }

        var _s   = "";
        var div  = parseInt(value.toString().length / 3);
        var rest = value.toString().length % 3;

        value = value.toString();

        if(value.length <= 3)
            return negative+value;

        _s = value.substring(value.length - 1) + _s;
        var countSeparator = 1;
        var _subs = value.length - 1;

        for (var i = value.length-2; i >= 0; i--) {
            _s = value.substring(i,_subs) + _s;
            _subs--;
        
            if(countSeparator == 2){
                countSeparator = 0;
                _s = separator + _s;
            }else{
                countSeparator++;
            }
        }
        _s = (_s.substring(1,0) == ',') ? _s.substring(1) : _s;

        return negative+_s;
    }
}

module.exports = ThousandsSeparatorSubtract;