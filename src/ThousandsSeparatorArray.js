const ThousandsSeparator = require('./ThousandsSeparator.js');

class ThousandsSeparatorArray extends ThousandsSeparator {
    format(value,separator){
        separator = super.checkSeparator(separator);
        if(super.checkValue(value) == 0)
            return 0;

        var negative = '';

        if(value[0] == '-'){
            negative = '-';
            value = value.substring(1);
        }

        var _s   = "";
        var div  = parseInt(value.toString().length / 3);
        var rest = value.toString().length % 3;

        value = value.toString();

        if(value.length == 3)
            return negative+value;

        var _array = [];

        _array[0]  = value[0];
        _array[0] += (rest == 2) ? value[1] : '';
        _array[0] += (rest == 0) ? value[1] + value[2] : '';

        var newValue   = value.substring(rest);
        var newPoint   = 0;
        if(rest == 0){
            var startArray = 0;
            div--;
        }else{
            var startArray = 1;
        }

        for (var i = startArray; i <= div; i++) {
            _array[i] = '';
            
            for (var y = newPoint; y < newPoint+3; y++) {
                _array[i] += newValue[y];
            }

            newPoint = y;
        }
        
        _s = _array.join(separator);
        return negative+_s;
    }
}

module.exports = ThousandsSeparatorArray;