import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Layout, Menu, Breadcrumb, Input, Row, Col, Tag } from 'antd';

import ThousandsSeparatorDivision from './ThousandsSeparatorDivision.js';
import ThousandsSeparatorRegExp   from './ThousandsSeparatorRegExp.js';
import ThousandsSeparatorArray    from './ThousandsSeparatorArray.js';
import ThousandsSeparatorSubtract from './ThousandsSeparatorSubtract.js';

const { Header, Content, Footer } = Layout;

class App extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            value:     null,
            result:    '',
            mode:      'RegExp',
            separator: ','
        };
    }

    formatValue(value = null, separator = null){
        switch (this.state.mode) {
            case 'RegExp':
                var ThousandsSeparator = new ThousandsSeparatorRegExp();
                break;
            case 'Array':
                var ThousandsSeparator = new ThousandsSeparatorArray();
                break;
            case 'Subtract':
                var ThousandsSeparator = new ThousandsSeparatorSubtract();
                break;
            default:
            case 'Division':
                var ThousandsSeparator = new ThousandsSeparatorDivision();
                break;
        }

        value     = (value == null) ? this.state.value : value.target.value;
        separator = (separator == null || separator == '') ? this.state.separator : separator;

        this.setState({
            value:  value,
            result: ThousandsSeparator.format(
                value,
                separator
            )
        });
    }

    setSeparator(value){
        this.formatValue(null,value.target.value);
        this.setState({
            separator: value.target.value,
        });
    }

    changeMode(value){
        this.setState({
            mode: value.key,
        });
    }

    render(){
        return (
            <Layout className="layout">
                <Header>
                    <div className="logo" />
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={[this.state.mode]}
                        style={{ lineHeight: '64px' }}
                        onSelect={this.changeMode.bind(this)}
                        >
                        <Menu.Item key="Division">Division</Menu.Item>
                        <Menu.Item key="RegExp">RegExp</Menu.Item>
                        <Menu.Item key="Array">Array</Menu.Item>
                        <Menu.Item key="Subtract">Subtract</Menu.Item>
                    </Menu>
                </Header>
                <Content style={{ padding: '0 50px' }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Raise Sistemas</Breadcrumb.Item>
                        <Breadcrumb.Item>Teste</Breadcrumb.Item>
                        <Breadcrumb.Item>Asafe Ramos</Breadcrumb.Item>
                    </Breadcrumb>
                    <div style={{ background: '#fff', minHeight: 280 }} className="contentArea">
                        <Row>
                            <Col span={24}>
                                <h2>Thousands Separator</h2>
                                <h4>Mode: <Tag color="#2db7f5">{this.state.mode}</Tag></h4>
                                <div>
                                    <Row>
                                        <Col span={11}>
                                            <Input addonBefore="Input" onChange={this.formatValue.bind(this)} />
                                        </Col>
                                        <Col span={11} offset={2}>
                                            <Input addonBefore="Separator" onChange={this.setSeparator.bind(this)} placeholder=","/>
                                        </Col>
                                    </Row>
                                </div>
                                <div>
                                    <Row>
                                        <Input addonBefore="Result" disabled={true} value={this.state.result}/>
                                    </Row>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                    Asafe Ramos | Ant Design + React.js
                    <br />
                    <span><a href="https://bitbucket.org/asaferamos/thousands-separator/" target="_blank">View in git repository</a></span>
                </Footer>
            </Layout>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
