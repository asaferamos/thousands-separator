class ThousandsSeparator {    
    checkValue(value){
        if(value == null)
            return 0

        if(typeof value === 'boolean' || typeof value === 'undefined')
            return 0

        if(value.toString().length > 15)
            return 0

        if (!isNaN(Number(value)))
            return value;

        return 0;
    }

    checkSeparator(separator){
        return (separator == null || separator == '') ? ',' : separator;
    }
}

module.exports = ThousandsSeparator;