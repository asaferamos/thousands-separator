const ThousandsSeparator = require('./ThousandsSeparator.js');

class ThousandsSeparatorRegExp extends ThousandsSeparator {
    format(value,separator){
        separator = super.checkSeparator(separator);
        if(super.checkValue(value) == 0)
            return 0;

        const regex = /(\d{3})/gm;
        var m;

        var negative = '';
        value = value.toString();

        if(value[0] == '-'){
            negative = '-';
            value = value.substring(1);
        }

        var _s   = "";
        var div  = parseInt(value.toString().length / 3);
        var normalize = value.toString().length % 3;

        
        if(normalize > 0)
            value = (normalize == 1) ? '00' + value : '0' + value;

        while ((m = regex.exec(value)) !== null) {

            m.forEach((match, groupIndex) => {
                if(groupIndex == 0)
                    _s += match + separator;
            });
        }

        _s = (normalize > 0) ? ((normalize == 1) ? _s.substring(2) : _s.substring(1)) : _s;
        _s = _s.substring(0,_s.length-(separator.length));

        return negative + _s;
    }
}

module.exports = ThousandsSeparatorRegExp;