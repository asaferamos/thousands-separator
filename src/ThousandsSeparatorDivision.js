const ThousandsSeparator = require('./ThousandsSeparator.js');

class ThousandsSeparatorDivision extends ThousandsSeparator {
    format(value,separator = null){
        separator = super.checkSeparator(separator);
        if(super.checkValue(value) == 0)
            return 0;

        var _s   = "";
        var div  = parseInt(value.toString().length / 3);
        var rest = value.toString().length % 3;

        value = value.toString();

        for (var i = 0; i < value.length; i++) {
            _s += value[i];
            if(_s == '-')
                continue;

            if(rest == 0 && (i+1) % 3 == 0 && i != (value.length - 1))
                _s += separator;
            
            if(div > 0 && rest > 0 && (i - rest + 1) % 3 == 0 && i != (value.length - 1))
                _s += separator;

        }
        return _s;
    }
}

module.exports = ThousandsSeparatorDivision;